<!DOCTYPE html>
<html>
<head>
 <title>Accueil</title>
 
<?php include_once("meta.html");?>
</head>

<body class="contener">
	<?php include_once("accueil.html");?> <!--header!-->


		<section class="accueil">
					<h1><hr>Présentations<hr></h1>
					
						<p>Pour la première fois en Isère, mais aussi la première fois dans un petit village authentique adossé au Vercors, Simplon.co propose une formation en développement web, en PHP, gratuite et intensive, de 7 mois à partir du mois d’avril 2017.</p>						
						<p> DEVELO'PONT est un projet initié par l'Espace Public Numérique du Royans (EPN) et SIMPLON.CO . C'est une formation GRATUITE, INTENSIVE, QUALIFIANTE de 7 mois en développement web et mobile. 
Notre mission est également de proposer des profils de poste adaptés aux entreprises et collectivités locales dans leur développement numérique. </p>

						<ul>Mené dans une démarche d’innovation sociale, ce projet constitue un double défi :
<li>économique : participer à l’endiguement du chômage en répondant aux besoins de recrutement des entreprises du numérique engagées dans cette démarche citoyenne,</li>
<li> social : en agissant sur l’emploi et la qualification de publics marginalisés sélectionnés uniquement sur la base de critères sociaux et de leur motivation, et non selon les diplômes.</li>
						</ul>
		
		
			<h1><hr>Actualités<hr></h1>

			
				<li>Semaine création du site internet dévelo'pont en incluant le PHP.</li>
				<li>Projet en binôme : reproduire un site déjà existant, le plus ressamblant possible.</li>
		</section>


	<?php include_once("coordonnees.html");?> <!--footer!-->


</body>
</html>