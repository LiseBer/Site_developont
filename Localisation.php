<!DOCTYPE html>
<html>
<head>
<?php include_once("meta.html");?>
</head>

<body class="contener">
	
	<?php include_once("accueil.html");?>




		<section class="PontenRoyans">
				<h1><hr>Pont en Royans<hr></h1>
					<img id="lieu" src="img/pontenroyans.jpg" alt="">
					<iframe id="iframe" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22545.29645370498!2d5.327738173425809!3d45.06222356287162!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478abb82046dcbe1%3A0xce9c6af3c67f8452!2s38680+Pont-en-Royans!5e0!3m2!1sfr!2sfr!4v1495199122353" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>		
					<p id="pont"> Pont-en-Royans, situé à l'entrée des Gorges de la Bourne, est le village reconnu comme l'un des bourgs les plus curieux en Dauphiné. Ses maisons suspendues aux façades colorées dominent
					 la Bourne, ses remparts, le pittoresque de ses ruelles, son pont. Ce bourg médiéval doit son charme à l'ingéniosité des hommes, qui dès le XVIème siècle, ont construit ce village perché pour favoriser 
					 la négoce du bois. Pont-en-Royans c'est aussi la rivière, ses berges améngées, le Musée de l'Eau, et la pêche.
					</p>
					
					<p>Le site des maisons suspendues au-dessus des eaux de la Bourne, inscrit aux titres des monuments historiques en 1944, avait impressionné Stendhal. Il faut l'admirer de la rive gauche de la Bourne. La visite du village médiéval, à travers le dédale de ses ruelles, peut agréablement se compléter par la montée pédestre à l'ancienne tour féodale des "Trois Châteaux" d'où l'on bénéficie d'un large panorama.</p>
					
		</section>
		
		<section class="Legrand">
					<h1><hr>Usine Legrand<hr></h1>
				
					<p>Legrand est un groupe industriel français historiquement implanté à Limoges dans le Limousin et un des leaders mondiaux des produits et systèmes pour installations électriques et réseaux d'information.
					</p>
					<li>Fondateurs : Edouard Decoster</li>
					<li>Produits : Prises et interrupteur principalement</li>
					
					<p>Accueillis gracieusement dans les locaux de l'usine Legrand, grâce à Laurent Besson, technicien du groupe et surtout bénévole de l'EPN qui, lui aussi, a fait un énorme travail dans la mise en place de la formation, les élèves ont découvert leurs locaux mis à disposition, trois espaces. En intervenant de cette façon, le groupe assure un mécénat d'entreprise plus qu'honorable que Line Rose Amozigh, présidente de l’EPN a voulu saluer lors de cette ouverture.</p>
		</section>
		
		<section>
		
	
		<?php include_once("coordonnees.html");?>

</body>
</html>