<!DOCTYPE html>
<html>	

<head>
<?php include_once("meta.html");?>
</head>

<body class="contener">

	<?php include_once("accueil.html");?>

		<section>
				<h1><hr>Le programme dans les grandes lignes<hr></h1>
				<ul>
					<li>Introduction</li>
					<li>Bloc 1 : Interface Utilisateurs</li>
					<li>Bloc 2 : POO PHP</li>
					<li>Bloc 3 : UX Design</li>
					<li>Bloc 4 : BDD</li>
					<li>Bloc 5 : Application client serveur</li>
					<li>Bloc 6 : Gestionnaire de package</li>
					<li>Bloc 7 : Préprocesseur</li>
					<li>Bloc 8 : Spécialisation</li>
				</ul>
		</section>
		
		<aside> 
			<h2>Horaires</h2>
			<ul>
				<li>Lundi : 9h-17h</li>
				<li>Mardi au Jeudi : 8h45-17h</li>
				<li>Vendredi : 8h45-16h</li>
			</ul>
		</aside>	
		

		<?php include_once("coordonnees.html");?>

	</div>
	</body>		
</html>